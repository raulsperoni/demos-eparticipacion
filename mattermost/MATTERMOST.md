## MATTERMOST

git clone https://github.com/mattermost/mattermost-docker.git

cp -rf mattermost.yml mattermost-docker/

cd mattermost-docker/

mkdir -p ./volumes/app/mattermost/{data,logs,config,plugins}
chown -R 2000:2000 ./volumes/app/mattermost/

docker-compose -f mattermost.yml up -d
