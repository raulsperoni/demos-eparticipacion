### DECIDIM

`git clone https://github.com/decidim/decidim
`

`cd decidim
`

`d/bundle install
`

Hay sustituir el `docker-compose.yml` por el que está en esta carpeta:


`d/rake development_app
`

Si esto da error que no encuentra la base de datos, levantar el contenedor manualmente:


`docker-compose up pg -d
`

Copiar rails-daemon a decidim/d/

`chmod +x rails-daemon
`

`d/rails-daemon server -b 0.0.0.0
`


User acting as an admin for the organization, with email admin@example.org and password decidim123456.


User that also belongs to the organization but it's a regular user, with email user@example.org and password decidim123456.
