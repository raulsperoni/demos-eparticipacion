### DISCOURSE

Seguir las instrucciones:

https://github.com/discourse/discourse/blob/master/docs/INSTALL-cloud.md

Modificar el container/app.yml para que funcione detrás del proxy:

Agregar al final:

```
##ADDED CONF


labels:
  app_name: discourse

  #----Traefik lables------------------------
  traefik.enable: true
  traefik.docker.network: proxy

   #---HTTP ROUTER SECTION-------------------
  #traefik.http.routers.discourse.rule: Host(`discourse.eparticipacion.magnesium.link`)
    #--HTTP SECTION--------------------------
  #traefik.http.routers.discourse.entrypoints: web
  #traefik.http.routers.discourse.middlewares: discourse_redirect2https         
  #traefik.http.services.discourse.loadbalancer.server.port: 80  

   #---HTTPS ROUTER SECTION
  traefik.http.routers.discourse_secure.rule: Host(`discourse.eparticipacion.magnesium.link`)
    #--HTTPS SECTION
  traefik.http.routers.discourse_secure.entrypoints: websecure
  traefik.http.services.discourse_secure.loadbalancer.server.port: 80
    #--TLS SECTION
  traefik.http.routers.discourse_secure.tls.certresolver: le
  traefik.http.routers.discourse_secure.tls: true

   #---MIDDLEWARE SECTION redirect http to https
  #traefik.http.middlewares.discourse_redirect2https.redirectscheme.scheme: https


docker_args:
  - "--network=proxy"
#  - "--expose=80"
```

```
./launcher rebuild app

```