# Etherpad-lite

## Requisitos

Para instalar esta herramienta se necesita instalar **docker** y **docker-compose**.

## Instalación

Dificultad: baja.

No es necesario instalar nada.

## Personalización

Se puede configurar tanto la interfaz como el comportamiento modificando las variables de entorno (indicadas [aquí](https://github.com/ether/etherpad-lite/blob/develop/doc/docker.md#options-available-by-default)) dentro del archivo `etherpad-lite/docker-compose.yml`.

Más personalización se puede llevar a cabo entrando en <http://0.0.0.0:9001/admin/settings>, por defecto las credenciales de administrador son:

* user: admin
* password: admin

## Ejecución

1. Iniciar contenedor de base de datos:

   ```bash
   docker-compose up -d db
   ```

2. Iniciar el resto de los contenedores:

   ```bash
   docker-compose up
   ```

Se ejecuta en la dirección <http://localhost:9001>.

## Bugs

No encontré.

## Sobre el proceso participativo

### Historial

Todos los cambios hechos quedan guardados en el historial y los usuarios los pueden ver (si se habilita).

### Comentarios

Con ayuda de un plugin se pueden agregar comentarios (estilo chat) sobre fragmentos del texto.
