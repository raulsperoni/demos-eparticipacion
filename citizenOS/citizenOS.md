# CitizenOS

## Requisitos

Para instalar esta herramienta se necesita instalar **docker** y **docker-compose**.

## Instalación

Dificultad: alta.

No es necesario instalar nada pero la configuración de despliegue es complicada y esta mal documentada (en algunos casos ni siquiera hay documentación).

## Personalización

Se puede configurar la interfaz dentro del repositorio citizenos-fe (la web).

También cuenta con configuraciones extras, como la conectividad con otros servicios, que deben ser especificadas dentro del archivo `local.json`. 

## Ejecución

1. Iniciar contenedores de base de datos:

   ```bash
   docker-compose up -d db.etherpad db.citizenos
   ```

2. Iniciar contenedor de etherpad:

   ```bash
   docker-compose up -d etherpad
   ```
   
3. Iniciar contenedor de la API:

   ```bash
   docker-compose up -d api.citizenos
   ```

4. Iniciar contenedor de la web:

   ```bash
   docker-compose up -d app.citizenos
   ```
