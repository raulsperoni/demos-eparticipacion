FROM postgres

WORKDIR /docker-entrypoint-initdb.d

RUN apt-get update && apt-get install -y curl && apt-get clean

RUN curl https://raw.githubusercontent.com/citizenos/citizenos-api/master/db/config/database.sql > database.sql
