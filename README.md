## Proxy Reverso

docker network create proxy
docker-compose -f traefik.yml up -d

## En cada servicio

En el compose para el servidor, NOMBRE_SERVICIO.yml:
1. Cada servicio web debe usar la red proxy de docker y se deben agregar labels para el proxy reverso cambiando NOMBRE_SERVICIO:

```
services:
  decidim:    
    networks:
      - proxy
    labels:
      - traefik.http.routers.NOMBRE_SERVICIO.rule=Host(`NOMBRE_SERVICIO.eparticipacion.magnesium.link`)
      - traefik.http.routers.NOMBRE_SERVICIO.tls=true
      - traefik.http.routers.NOMBRE_SERVICIO.tls.certresolver=le


networks:
  proxy:
    external: true

```
2. Los nombres de servicios deben ser unicos, por ejemplo evitar el pg, y poner pg_NOMBRE_SERVICIO

3. Evitar mapear puertos para que no haya colisiones, si en necesario mapear dejar que docker asigne uno aleatorio. En lugar de:

```
    ports:
      - 3000:3000
```
Hacer:
```
    ports:
      - 3000
```
docker-compose -f=NOMBRE_SERVICIO.yml up -d
